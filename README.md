This is a kata created in my local group of software craftmanship with Borja Guillén.

The starting point was the test set provided by Sandi Metz https://github.com/sandimetz/99bottles-polyglot

From there we take TDD to flesh out the project.