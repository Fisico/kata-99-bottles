package org.kata;

public interface Verse {
    /**
     *
     * @return La representacion de la linea del verso de la cancion de la cerveza
     */
    String getVerse();
}
