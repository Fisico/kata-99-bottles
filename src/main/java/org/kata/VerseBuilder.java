package org.kata;

public class VerseBuilder {

    private static final int THIRD_TO_LAST = 2;
    private static final int PENULTIMATE = 1;
    private static final int ULTIMATE = 0;

    public Verse buildVerse(int verseNumber) {
        if (verseNumber == PENULTIMATE) {
            return new PenultimateVerse();
        } else if (verseNumber == THIRD_TO_LAST) {
            return new ThirdLastVerse();
        } else if(verseNumber == ULTIMATE) {
            return new UltimateVerse();
        } else {
            return new RegularVerse(verseNumber);
        }
    }
}
