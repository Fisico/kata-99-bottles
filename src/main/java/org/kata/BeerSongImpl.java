package org.kata;

public class BeerSongImpl implements BeerSongInterface {

    public String verse(int verseNumber) {
        Verse verse = new VerseBuilder().buildVerse(verseNumber);
        return verse.getVerse();
    }

    public String sing(int firstVerse, int lastVerse) {
        StringBuffer song = new StringBuffer();
        for(int index = firstVerse; index >= lastVerse; index--) {
            song.append(verse(index));
        }
        return song.toString();
    }

    public String singSong() {
        return sing(99,0);
    }
}
