package org.kata;

public interface BeerSongInterface {
    String verse(int verseNumber);

    String sing(int firstVerse, int lastVerse);

    String singSong();
}
