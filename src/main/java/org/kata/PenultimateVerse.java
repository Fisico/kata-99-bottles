package org.kata;

public class PenultimateVerse implements Verse {
    public String getVerse() {
        return "1 bottle of beer on the wall, 1 bottle of beer.\n" +
                "Take it down and pass it around, no more bottles of beer on the wall.\n\n";
    }
}
