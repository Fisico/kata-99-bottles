package org.kata;

public class RegularVerse implements Verse {

    private final int numVerse;

    public RegularVerse(int numVerse) {
        this.numVerse = numVerse;
    }

    public String getVerse() {
        StringBuffer verse = new StringBuffer();
        verse.append(numVerse);
        verse.append(" bottles of beer on the wall, ");
        verse.append(numVerse);
        verse.append(" bottles of beer.\n");
        verse.append("Take one down and pass it around, ");
        verse.append(numVerse - 1);
        verse.append(" bottles of beer on the wall.\n\n");
        return verse.toString();
    }
}
