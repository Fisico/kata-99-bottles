package org.kata;

public class ThirdLastVerse implements Verse {
    public String getVerse() {
        return "2 bottles of beer on the wall, 2 bottles of beer.\n" +
                "Take one down and pass it around, 1 bottle of beer on the wall.\n\n";
    }
}
